### Approximate a Circular arc with a Bezier curve

Circular arc:
```math
\mathbf{c}(t)=\left\{\begin{matrix}
R \cos\left ( \theta t \right )\\ 
R \sin\left ( \theta t \right )
\end{matrix}\right. ,\ \   0\leq t\leq 1.
```

Bezier curve:
```math
\mathbf {B} (t)=(1-t)^{2}\mathbf {P} _{0}+2(1-t)t\mathbf {P} _{1}+t^{2}\mathbf {P} _{2},\ \   0\leq t\leq 1.
```
with $`\mathbf{P}_0=(1,0)`$, $`\mathbf{P}_2=(R \cos \theta,R \sin\theta)`$ and
```math
\mathbf{P}_1=R\left(2-\cos\frac{\theta}{2}\right)\cdot\left( \cos \frac{\theta}{2},\sin\frac{\theta}{2} \right)
```
A good approximation when $`\theta\ll 1`$.
