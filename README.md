# Projections in Computer Graphics

Mathematics to create the illusion of depth in a two-dimensional drawing.

## The Mathematics of Perspective

> From David Salomon book `Transformations and Projections in Computer Graphics-Springer (2006)`, chapter 3

The problem involves three entities, a (three-dimensional) object to be projected, a projection plane, and a viewer watching the projection on this plane. The object and the viewer are located on different sides of the projection plane, and the problem is to determine what the viewer will see on the plane. It is like having a transparent plane and looking through it at an object. Specifically, given an arbitrary point $`\mathbf{P} = (x, y, z)`$ on the object, we want to compute the two-dimensional coordinates $`(x^*, y^*)`$ of its projection $`\mathbf{P^*}`$ on the projection plane.
Once this is done for all the points of the object, the perspective projection of the object appears on the projection plane. Thus, the problem is to find a transformation T that will transform $`\mathbf{P}`$ to $`\mathbf{P^*}`$:
```math
\mathbf{P^*}=\mathbf{T}\mathbf{P}.
```
The viewer and the object have to be located on different sides of the plane, and the viewer should look at the plane. Thus, the mathematical expressions for perspective must depend on the location and orientation of the viewer and the projection plane, as well as on the location of each point $`\mathbf{P}`$ on the object.

### The standard position
The viewer is positioned at $`(0,0,-k)`$ and looks in the positive $`z`$ axis, so the line of sight is the vector $`(0,0,1)`$. The projection plane is the $`xy`$ plane. All the points of the object must have non-negative $`z`$ coordinates.

The projection is 
```math
\begin{matrix}
x^*=\dfrac{x}{\frac{z}{k}+1}\\ 
y^*=\dfrac{y}{\frac{z}{k}+1}
\end{matrix}
```
This projection can be included in the $`4\times 4`$ transformation matrix in three dimensions:
```math
\mathbf{T}_p=\begin{pmatrix}
1 &0  &0  &0 \\ 
0 &1  &0  &0 \\ 
 0&0  &0  &0 \\ 
 0&0  &1/k  &1 
\end{pmatrix}.
```

### A viewer standing off-axis
Suppose the viewer is positioned at
```math
\mathbf{B}=\left(x_0,y_0,-k\right).
```
The projection plane is again the $`xy`$ plane, but the transformation matrix include an additional translation element. The projection is 
```math
\begin{matrix}
x^*=\dfrac{x+\frac{x_0}{k}z}{\frac{z}{k}+1}\\ 
y^*=\dfrac{y+\frac{y_0}{k}z}{\frac{z}{k}+1}
\end{matrix}
```
which again can be written as a $`4\times 4`$ transformation matrix in three dimensions:
```math
\mathbf{T}_p=\begin{pmatrix}
1 &0  &\frac{x_0}{k}  &0 \\ 
0 &1  &\frac{y_0}{k}  &0 \\ 
 0&0  &0  &0 \\ 
 0&0  &1/k  &1 
\end{pmatrix}.
```

### General Perspective
Next question in generalising this problem is to find the projection of the object when the projection plane is not the $`xy`$ plane, but the plane $`\alpha x +\beta y+\gamma z=0`$. I.e., the viewer is not looking in the $`z`$ direction. The solution is to transform the object, not the viewer, because in practice we want to display the projected points on the screen, which is two-dimensional. Leave the viewer at the standard position and transform the object instead. See section 3.6 in the book.

In order to derive the mathematics of general perspective, we need to know at least
1. the location $`\mathbf{B}`$ of the viewer
2. the direction $`\mathbf{D}`$ of the viewer’s line of sight
3. the coordinates of all the points $`\mathbf{P}`$ on the object.

In order to fully specify the viewer-screen unit, we sometimes also need to specify the direction $`\mathbf{T}`$ of the top of the screen. Viewers may be located at the same point and looking in the same direction, but with screens that have different orientations (although each is perpendicular to the line of sight).


## GeoGebra demo
```
k=[0,8] % slider
A,B,C=point in 3d
v=(0,0,-k)
a=((x(A))/((z(A))/(k)+1),(y(A))/((z(A))/(k)+1),0)
b= same
c=same
```
now draw a triangle made from these 3 points, (ABC and abc). The projection is inverted in the 2D image plane. How to solve it when working in right-handed coordinate system?


To solve this, just let y -> -y of the projected points.

There is something else that doesn't seem correct: As k gets larger, the triangle becomes larger, and as k gets smaller, the traingle becomes smaller.
Shouldn't it be the opposite?
